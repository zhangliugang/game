/*
 * PlaneLayer.h
 *
 *  Created on: 2014年8月25日
 *      Author: zhangliugang
 */

#ifndef PLANELAYER_H_
#define PLANELAYER_H_

#include "cocos2d.h"
#include "PigSprite.h"

USING_NS_CC;

enum Enum_Plane {
    AIRPLANE = 1,
};

class PlaneLayer: public Layer {
public:
	PlaneLayer();
	virtual ~PlaneLayer();
	static Scene *createScene();
	virtual bool init();
	void menuCloseCallBack(Ref *pSender);
	static PlaneLayer *create();
public:
	PigSprite *mp_pig;
	static PlaneLayer *sharedPlane;
	void f_createSprite();
public:
	bool onTouchBegan(Touch *touch, Event *pEvent);
	void onTouchMoved(Touch *touch, Event *pEvent);
};

#endif /* PLANELAYER_H_ */
