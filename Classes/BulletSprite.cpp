//
//  BulletSprite.cpp
//  MyCppGame
//
//  Created by zhangliugang on 14-8-25.
//
//

#include "BulletSprite.h"
#include "PlaneLayer.h"

USING_NS_CC;

BulletSprite::BulletSprite() {}
BulletSprite::~BulletSprite() {}

bool BulletSprite::init() {
    bool pRet = false;
    do {
        CC_BREAK_IF(!Layer::init());
        Texture2D *texture = TextureCache::sharedTextureCache()->textureForKey("bullet.png");
        bulletBatchNode = SpriteBatchNode::createWithTexture(texture);
        this->addChild(bulletBatchNode);
        this->schedule(schedule_selector(BulletSprite::shootbullet), 0.2f);
        pRet = true;
    }while(0);
    return pRet;
}

void BulletSprite::shootbullet(float dt) {
    Size winSize = Director::getInstance()->getWinSize();
    auto PlanePos = PlaneLayer::sharedPlane->getChildByTag(AIRPLANE)->getPosition();
    
    auto spriteBullet = Sprite::createWithSpriteFrameName("bullet_1.png");
    bulletBatchNode->addChild(spriteBullet);
    vecBullet.pushBack(spriteBullet);
    Point bulletPos = Point(PlanePos.x, PlanePos.y + PlaneLayer::sharedPlane->getChildByTag(AIRPLANE)->getContentSize().height / 2 + 20);
    spriteBullet->setPosition(bulletPos);
    spriteBullet->setScale(0.4f);
    
    float flyLen = winSize.height - PlanePos.y;
    float flyVelocity = 320 / 1;
    float realFlyDuration = flyLen / flyVelocity;
    
    auto actionMove = MoveTo::create(realFlyDuration, Point(bulletPos.x, winSize.height));
    auto actionDone = CallFuncN::create(CC_CALLBACK_1(BulletSprite::removeBullet, this));
    
    Sequence *sequeue = Sequence::create(actionMove, actionDone, NULL);
    spriteBullet->runAction(sequeue);
}

void BulletSprite::removeBullet(cocos2d::Node *pNode) {
    if (NULL == pNode) {
        return;
    }
    Sprite *bullet = (Sprite*)pNode;
    this->bulletBatchNode->removeChild(bullet, true);
    vecBullet.eraseObject(bullet);
}

Animation *BulletSprite::f_createAnimate(int count, int fps) {
    char buff[16];
    Animation *panimation = Animation::create();
    panimation->setDelayPerUnit(1.0f / fps);
    for (int id = 1; id <= count; id++) {
        sprintf(buff, "bullet_%d.png",id);
        panimation->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(buff));
    }
    return panimation;
}