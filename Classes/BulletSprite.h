//
//  BulletSprite.h
//  MyCppGame
//
//  Created by zhangliugang on 14-8-25.
//
//

#ifndef __MyCppGame__BulletSprite__
#define __MyCppGame__BulletSprite__

#include "cocos2d.h"

USING_NS_CC;

class BulletSprite : public Layer
{
public:
    BulletSprite();
    ~BulletSprite();
    virtual bool init();
    CREATE_FUNC(BulletSprite);
    
    Animation *f_createAnimate(int count, int fps);
    void removeBullet(Node *pNode);
    void shootbullet(float dt);
public:
    Vector<Sprite*> vecBullet;
    SpriteBatchNode *bulletBatchNode;
};

#endif /* defined(__MyCppGame__BulletSprite__) */
