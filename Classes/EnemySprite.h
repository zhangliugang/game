//
//  EnemySprite.h
//  MyCppGame
//
//  Created by zhangliugang on 14-8-25.
//
//

#ifndef __MyCppGame__EnemySprite__
#define __MyCppGame__EnemySprite__

#include "cocos2d.h"
#include "EnemyInfo.h"

USING_NS_CC;

class EnemySprite : public Node
{
public:
    EnemySprite();
    ~EnemySprite();
    virtual bool init();
    CREATE_FUNC(EnemySprite);
public:
	void setEnemyByType(EnemyType enType);
	Sprite *getSprite();
    int getLife();
    void loseLife();
    Rect getBoundingBox();
    Point getcurPoint();
private:
    Sprite *pEnemySprite;
    int nLife;
};

#endif /* defined(__MyCppGame__EnemySprite__) */
