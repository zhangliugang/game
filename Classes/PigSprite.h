//
//  PigSprite.h
//  MyCppGame
//
//  Created by zhangliugang on 14-8-25.
//
//

#ifndef __MyCppGame__PigSprite__
#define __MyCppGame__PigSprite__

#include "cocos2d.h"
USING_NS_CC;

class PigSprite : public Sprite
{
public:
    PigSprite();
    ~PigSprite();
    virtual bool init();
    CREATE_FUNC(PigSprite);
    void f_createAnimate(int count, int fps);
    void f_followPlane(float dt);
    Sprite *spritePig;
};

#endif /* defined(__MyCppGame__PigSprite__) */
