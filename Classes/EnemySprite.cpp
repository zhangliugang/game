//
//  EnemySprite.cpp
//  MyCppGame
//
//  Created by zhangliugang on 14-8-25.
//
//

#include "EnemySprite.h"

USING_NS_CC;

EnemySprite::EnemySprite() {};
EnemySprite::~EnemySprite() {};

void EnemySprite::setEnemyByType(EnemyType enType) {
    switch (enType) {
        case Enemy1:
            pEnemySprite = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("n1.png"));
            nLife = ENEMY1_MAXLIFE;
            break;
        case Enemy2:
            pEnemySprite = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("n2.png"));
            nLife = ENEMY2_MAXLIFE;
            break;
        case Enemy3:
            pEnemySprite = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("n3.png"));
            nLife = ENEMY2_MAXLIFE;
            break;
        case Enemy4:
            pEnemySprite = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("n_boss.png"));
            nLife = ENEMY3_MAXLIFE;
            break;
        default:
            return;
            break;
    }
    this->addChild(pEnemySprite);
    pEnemySprite->setScale(0.6f);
    Size winSize = Director::getInstance()->getWinSize();
    Size enemySize = pEnemySprite->getContentSize();
    //random
    int minX = enemySize.width / 2;
    int maxX = winSize.width - enemySize.width / 2;
    int actualX = (rand() % (maxX - minX)) + minX;
    this->setPosition(Point(actualX, winSize.height + enemySize.height / 2));
}

bool EnemySprite::init() {
    bool pRet = true;
    if (!Node::init()) {
        pRet = false;
    }
    return pRet;
}

Sprite *EnemySprite::getSprite() {
    return pEnemySprite;
}

int EnemySprite::getLife() {
    return nLife;
}

void EnemySprite::loseLife() {
    nLife -= 0.5;
}

Point EnemySprite::getcurPoint() {
    return this->getPosition();
}

Rect EnemySprite::getBoundingBox() {
    Rect rect = pEnemySprite->getBoundingBox();
    Point pos = this->convertToWorldSpace(rect.origin);
    Rect enemRect(pos.x, pos.y, rect.size.width, rect.size.height);
    return enemRect;
}