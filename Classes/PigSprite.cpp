//
//  PigSprite.cpp
//  MyCppGame
//
//  Created by zhangliugang on 14-8-25.
//
//

#include "PigSprite.h"
#include "PlaneLayer.h"

USING_NS_CC;

PigSprite::PigSprite() {}
PigSprite::~PigSprite() {}

bool PigSprite::init() {
    if (!Sprite::init()) {
        return false;
    }
    Size winSize = Director::getInstance()->getWinSize();
    spritePig = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("hero_01.png"));
    this->setPosition(Point(winSize.width / 2, winSize.height / 2));
    this->addChild(spritePig);
    this->setScale(0.6);
    f_createAnimate(3, 8);
    this->schedule(schedule_selector(PigSprite::f_followPlane));
    return true;
}

void PigSprite::f_followPlane(float dt) {
	Size winSize = Director::getInstance()->getWinSize();
	auto PlanePos = PlaneLayer::sharedPlane->getChildByTag(AIRPLANE)->getPosition();
}

void PigSprite::f_createAnimate(int count, int fps) {
	char buff[16];
	Animation *panimation = Animation::create();
	panimation->setDelayPerUnit(1.0/fps);
	for (int id = 1; id <= count; ++id) {
		sprintf(buff, "hero_0%d.png", id);
		panimation->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(buff));
	}
	spritePig->runAction(RepeatForever::create(Animate::create(panimation)));
}
