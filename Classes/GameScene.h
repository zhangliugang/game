//
//  GameScene.h
//  MyCppGame
//
//  Created by zhangliugang on 14-8-25.
//
//

#ifndef __MyCppGame__GameScene__
#define __MyCppGame__GameScene__

#include "cocos2d.h"
#include "PlaneLayer.h"
#include "BulletSprite.h"
#include "EnemyLayer.h"

USING_NS_CC;

class GameLayer : public Layer {
public:
    static Scene *createScene();
    virtual bool init();
    virtual void onEnterTransitionDidFinish();
    CREATE_FUNC(GameLayer);
public:
    void gameUpdate(float dt);
    bool bulletCollisionEnemy(Sprite *pBullet);
    bool enemyCollisionPlane();
    void menuCloseCallback(Ref *pSender);
public:
    PlaneLayer *planeLayer;
    BulletSprite *bulletSprite;
    EnemyLayer *enemyLayer;
    
    int getRand(int start, int end);
};

#endif /* defined(__MyCppGame__GameScene__) */
